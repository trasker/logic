from connection import LC, Connector
from gates import XOR, AND, OR


class AdderError(RuntimeError):
    pass


class HalfAdder(LC):

    def __init__(self, name: str) -> None:
        super().__init__(name=name)
        self.I1 = Connector(parent=self,name='I1',activates=1)
        self.I2 = Connector(parent=self,name='I2',activates=1)
        self.S = Connector(parent=self,name='Sum')
        self.C = Connector(parent=self,name='Carry')
        self.X1 = XOR(name="X1")
        self.A1 = AND(name="A1")
        self.I1.connect(inputs=[self.X1.I1, self.A1.I1])
        self.I2.connect(inputs=[self.X1.I2, self.A1.I2])
        self.X1.OUT.connect(inputs=[self.S])
        self.A1.OUT.connect(inputs=[self.C])


class FullAdder(LC):

    def __init__(self, name: str):
        super().__init__(name=name)
        self.I1 = Connector(parent=self, name='I1', activates=1)
        self.I2 = Connector(parent=self, name='I2', activates=1)
        self.Cin = Connector(parent=self, name='Cin', activates=1)
        self.S = Connector(parent=self, name='Sum')
        self.Cout = Connector(parent=self, name='Cout')
        self.H1 = HalfAdder(name="H1")
        self.H2 = HalfAdder(name="H2")
        self.O1 = OR(name="O1")
        self.I1.connect([self.H1.I1])
        self.I2.connect([self.H1.I2])
        self.Cin.connect([self.H2.I1])
        self.H1.S.connect([self.H2.I2])
        self.H1.C.connect([self.O1.I2])
        self.H2.C.connect([self.O1.I1])
        self.H2.S.connect([self.S])
        self.O1.OUT.connect([self.Cout])


class NBitAdder(LC):
    
    def __init__(self, name: str, n_bits: int=4) -> None:
        super().__init__(name=name)
        self.adders = [FullAdder(name=f"F{i}") for i in range(n_bits)]
        for add_1, add_2 in zip(self.adders[:-1], self.adders[1:]):
            add_1.Cout.connect(add_2.Cin)
        self.n_bits = n_bits

    def bit(self, x: str, bit: int) -> bool:
        return int(x[bit] == '1')

    def __call__(self, a: str, b: str) -> str:
        if not len(a) == self.n_bits or not len(b) == self.n_bits:
            raise AdderError(f"Wrong number of bits, len(a)={len(a)}, " + 
                             f"len(b)={len(b)}, " +
                             f" should be n_bits={self.n_bits}")
        self.adders[0].Cin.set(0)
        for i, adder in enumerate(self.adders):
            adder.I1.set(self.bit(a, self.n_bits - (i + 1)))
            adder.I2.set(self.bit(b, self.n_bits - (i + 1)))
        
        return "".join(map(str, [self.adders[-1].Cout.value] + 
            [adder.S.value for adder in self.adders[::-1]]))


