import pytest
from gates import AND, OR, NOT, XOR
from adders import HalfAdder, FullAdder, NBitAdder, AdderError
from connection import ConnectorError


def test_AND():
    a = AND(name="and")
    with pytest.raises(ConnectorError):
        a.I1.set(2)
    a.I1.set(1)
    a.I2.set(1)
    assert a.OUT.value == 1
    a.I2.set(0)
    assert a.OUT.value == 0


def test_OR():
    a = OR(name="or")
    a.I1.set(1)
    assert a.OUT.value == 1
    a.I2.set(1)
    a.I1.set(0)
    assert a.OUT.value == 1
    a.I2.set(0)
    assert a.OUT.value == 0


def test_NOT():
    a = NOT(name="not")
    a.IN.set(1)
    assert a.OUT.value == 0
    a.IN.set(0)
    assert a.OUT.value == 1


def test_XOR():
    xor = XOR(name="xor")
    assert xor.OUT.value == 0
    xor.I2.set(1)
    assert xor.OUT.value == 1
    xor.I1.set(1)
    assert xor.OUT.value == 0
    xor.I2.set(0)
    assert xor.OUT.value == 1


def test_HalfAdder():
    h = HalfAdder(name="H")
    assert h.C.value == 0
    assert h.S.value == 0
    h.I1.set(1)
    assert h.S.value == 1
    assert h.C.value == 0
    h.I2.set(1)
    assert h.S.value == 0
    assert h.C.value == 1
    

def test_FullAdder():
    f = FullAdder(name="F")
    assert f.S.value == 0
    assert f.Cout.value == 0
    f.I1.set(1)
    f.I2.set(0)
    assert f.S.value == 1
    assert f.Cout.value == 0
    f.I1.set(0)
    f.I2.set(1)
    assert f.S.value == 1
    assert f.Cout.value == 0
    f.I1.set(1)
    assert f.S.value == 0
    assert f.Cout.value == 1
    f.Cin.set(1)
    assert f.S.value == 1
    assert f.Cout.value == 1


def test_NBitAdder():
    n = NBitAdder(name="n", n_bits=4)
    a = "0001"
    b = "0001"
    res = n(a, b) 
    assert res == "00010"
    with pytest.raises(AdderError):
        a = "00010"
        res = n(a, b)
    a = "1111"
    b = "1111"
    res = n(a, b)
    assert res == "11110" 
    n = NBitAdder(name="n", n_bits=8)
    a = "10000000"
    b = "00000001"
    res = n(a, b)
    assert res == "010000001"
