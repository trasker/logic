from connection import Connector, LC


class BaseGate(LC):

    def __init__(self, *, name: str) -> None:
        super().__init__(name=name)
        self.I1 = Connector(parent=self, name="I1", activates=True)
        self.I2 = Connector(parent=self, name="I2", activates=True)
        self.OUT = Connector(parent=self, name="OUT")

    @property
    def evaluate(self): pass
    
    @property
    def initialise(self):
        [i.set(1) for i in [self.I1, self.I2, self.OUT]]
        [i.set(0) for i in [self.I1, self.I2, self.OUT]]

class NOT(LC):

    def __init__(self, *, name) -> None:
        super().__init__(name=name)
        self.IN = Connector(parent=self, name="IN", activates=1)
        self.OUT = Connector(parent=self, name="OUT")

    @property
    def evaluate(self) -> None: self.OUT.set(not self.IN.value)


class AND(BaseGate):
    
    def __init__(self, *, name: str) -> None:
        super().__init__(name=name)
        self.initialise
    
    @property
    def evaluate(self) -> None: self.OUT.set(self.I1.value and self.I2.value)


class OR(BaseGate):

    def __init__(self, *, name: str) -> None:
        super().__init__(name=name)
        self.initialise

    @property
    def evaluate(self) -> None: self.OUT.set(self.I1.value or self.I2.value)

class XOR(BaseGate):
    def __init__ (self, *, name) -> None:
        super().__init__(name=name)
        self.A1 = AND(name="A1") 
        self.A2 = AND(name="A2")
        self.N1 = NOT(name="I1")
        self.N2 = NOT(name="I2")
        self.O1 = OR(name="O1")
        self.I1.connect(inputs=[self.A1.I1, self.N2.IN])
        self.I2.connect(inputs=[self.N1.IN, self.A2.I1])
        self.N1.OUT.connect(inputs=[self.A1.I2])
        self.N2.OUT.connect(inputs=[self.A2.I2])
        self.A1.OUT.connect(inputs=[self.O1.I1])
        self.A2.OUT.connect(inputs=[self.O1.I2])
        self.O1.OUT.connect(inputs=[self.OUT])
        self.initialise

