class ConnectorError(RuntimeError):
    pass


class Connector:

    def __init__(self, *,
                 parent: "LC",
                 name: str,
                 monitor=False,
                 activates=False) -> None:
        self._value = 0
        self._parent = parent
        self._name = name
        self.monitor = monitor
        self.activates = activates
        self.connects = []

    @property
    def parent(self) -> "LC": return self._parent

    @property
    def name(self) -> str: return self._name

    @property
    def value(self) -> int:
        return self._value
    
    def connect(self, inputs: list) -> None:
        if not isinstance(inputs, list):
            inputs = [inputs]
        for i in inputs:
            self.connects.append(i)

    def set(self, value: int) -> None:
        if isinstance(value, bool):
            value = int(value)
        if self.value == value:
            return
        elif not value == 1 and not value == 0:
            raise ConnectorError(f"Connector {self}, only binary values allowed")
        self._value = value
        if self.activates:
            self.parent.evaluate
        if self.monitor:
            print(f"Connector {self.parent.name} - {self.name} set to {self.value}")
        for c in self.connects:
            c.set(value)


class LC:

    def __init__(self, *, name: str) -> None:
        self._name = name

    @property
    def name(self) -> str: return self._name
    
    @property
    def evaluate(self) -> None: pass
